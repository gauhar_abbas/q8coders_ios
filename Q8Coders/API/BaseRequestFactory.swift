//
//
//  Created by Gauhar Abbas on 01/12/18.
//  Copyright © 2018 Q8Coders. All rights reserved.
//
import Foundation
import Alamofire

protocol BaseRequestFactory {
    var networkManager : NetworkManager {get set}

}

extension BaseRequestFactory {
    
    public func encodeRequest(with parameters: [String: Any],
                              endPoint: String,
                              httpMethod: HTTPMethod = .post) -> URLRequest? {
        
        do {
            if var requestURL = self.networkManager.baseURL {
              
                requestURL.appendPathComponent(endPoint)
                var request = URLRequest(url: requestURL, cachePolicy: .reloadIgnoringCacheData,
                                         timeoutInterval: NetworkManager.timeoutInterval)
                request.httpMethod = httpMethod.rawValue
                
                return try URLEncoding.httpBody.encode(request, with: parameters)
            }
        } catch _ {
            
        }
        return nil
    }
}



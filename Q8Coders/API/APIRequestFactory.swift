//
//
//  Created by Gauhar Abbas on 01/12/18.
//  Copyright © 2018 Q8Coders. All rights reserved.
//
import Foundation
public class APIRequestFactory: BaseRequestFactory {
  
  var networkManager: NetworkManager
  
  internal required init(_ networkManager: NetworkManager) {
    self.networkManager = networkManager
  }
}

// Auth related to Request
extension APIRequestFactory {
  
  func register(country: String, phone: String) -> URLRequest?  {
    
    let parameters = [APIParams.phone.rawValue: country+phone]
    return encodeRequest(with: parameters, endPoint: APIEndPoint.register.rawValue, httpMethod: .post)
    
  }
  
  func verify(phone: String, code: String) -> URLRequest?  {
    
    let parameters = [APIParams.phone.rawValue: phone,
                      APIParams.code.rawValue: code]
    return encodeRequest(with: parameters, endPoint: APIEndPoint.verify.rawValue, httpMethod: .post)
  }
  
  func addNewCarts(name: String,ownerId:String) -> URLRequest?  {
    
    let parameters = [APIParams.name.rawValue: name,
                      APIParams.owner_id.rawValue: ownerId]
    return encodeRequest(with: parameters, endPoint: APIEndPoint.carts.rawValue, httpMethod: .post)
  }
  func getCartsList(member_id: String,pageNo:String) -> URL?  {
    
    let endPoints =
      APIEndPoint.members.rawValue+member_id+"/"+APIEndPoint.carts.rawValue+"?page=\(pageNo)"
    return URL(string: "https://cart.coderswebsites.com/"+endPoints)
  }
}


//
//  Logger.swift
//
//  Created by Gauhar Abbas on 01/12/18.
//  Copyright © 2018 Q8Coders. All rights reserved.
//

import Foundation

public enum APIEndPoint : String {
    
    case register = "registration/"
    case verify = "verification/"
    case carts = "carts/"
    case members = "members/"
}

public enum APIParams : String {
    case code = "code"
    case phone = "phone"
    case owner_id = "ownerId"
    case name = "name"
    case page_no = "page"
}

public enum AppEnvironment : String {
  
  case stagEnvironment
  case prodEnvironment
  case none
  
  func getServer() -> String? {
    
    switch self {
    case .stagEnvironment:
      return "https://cart.coderswebsites.com/"
    case .prodEnvironment:
      return "https://cart.coderswebsites.com/"
    default:
      return nil
    }
  }
}


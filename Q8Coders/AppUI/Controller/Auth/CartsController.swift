//
//  ForgorController.swift
//  Elixir
//
//  Created by Afgan on 10/14/18.
//  Copyright © 2018 Elixir. All rights reserved.
//

import UIKit
import MBProgressHUD
class CartsController: UIViewController,
                       UITableViewDataSource,
                       UITableViewDelegate{
  
    @IBOutlet var cartListTable:UITableView!
    var cartDataArray = [NSDictionary]() {
     didSet{cartListTable.reloadData()} // reload data on pagination
    }
    var pageNo = 1 // page no for pagination for more data
    let spinner: UIActivityIndicatorView = UIActivityIndicatorView.init(style:.gray)

  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.navigationBar.isHidden = true
    cartListTable.tableFooterView = UIView()
    // owner id recieved at registration
    let ownerID = UserDefaults.standard.value(forKey: "owner_id") as? Int ?? 0
    MBProgressHUD.showAdded(to: view, animated: true)
    AccountManager.inst.getCartList(member_id: "\(ownerID)", page_no: "\(1)", success: { (response) in
      Logger.log.debug("SUCCESS :: \(response)")
      MBProgressHUD.hide(for: self.view, animated: true)
      let data = response.rawValue as? NSDictionary ?? [:]
      let results = data["results"] as? [NSDictionary] ?? []
      self.cartDataArray = results
    }) { (error) in
      MBProgressHUD.hide(for: self.view, animated: true)
        self.view.makeToast(error == nil ? "Internet Connection Error" : "Invalid request")
      Logger.log.debug("ERROR :: \(String(describing: error))")
    }
  }
    // push to add cart screen
  @IBAction func addCartButtonTapped(sender: UIBarButtonItem) {
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let createCartController = storyBoard.instantiateViewController(withIdentifier: "CreateCartController") as! CreateCartController
    createCartController.callback = addCartCallback
    navigationController?.pushViewController(createCartController, animated: true)
  }
    //callback from add cart screen after adding a cart
  func addCartCallback(cartName:NSDictionary){
    cartDataArray = cartDataArray + [cartName]
  }
  
    //tableview delegate functions
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cartDataArray.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cartCell") as! CartCell
    let tempDic = cartDataArray[indexPath.row]
    cell.cartName.text = tempDic["name"] as? String ?? ""
    return cell
  }
    // pagination will get 15 more data on table scroll
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("scrollViewDidEndDragging")
        if ((cartListTable.contentOffset.y + cartListTable.frame.size.height) >= cartListTable.contentSize.height){
            if cartDataArray.count >= 15 {
                if pageNo != cartDataArray.count/15{return}
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: cartListTable.bounds.width, height: CGFloat(44))
                cartListTable.tableFooterView = spinner
                spinner.hidesWhenStopped = true
                cartListTable.tableFooterView?.isHidden = false
                pageNo = pageNo+1
                spinner.startAnimating()
                let ownerID = UserDefaults.standard.value(forKey: "owner_id") as? Int ?? 0
                AccountManager.inst.getCartList(member_id: "\(ownerID)", page_no: "\(pageNo)", success: { (response) in
                    Logger.log.debug("SUCCESS :: \(response)")
                    self.spinner.stopAnimating()
                    let data = response.rawValue as? NSDictionary ?? [:]
                    let results = data["results"] as? [NSDictionary] ?? []

                    //self.cartDataArray.addingObjects(from: results as! [Any])
                    self.cartDataArray = self.cartDataArray + results
                }) { (error) in
                    self.spinner.stopAnimating()
                    self.view.makeToast(error == nil ? "Internet Connection Error" : "Invalid request")
                    Logger.log.debug("ERROR :: \(String(describing: error))")
                }
            }
        }
    }
    
    //logout user so that user dnt face issue to use app for diffrent user
  @IBAction func logoutUser(_ sender:UIButton){
    UserDefaults.standard.removeObject(forKey: "isLoggedIn")
    let delegate = UIApplication.shared.delegate as! AppDelegate
    delegate.appInitializer?.rootWireframe.showLoginModuleAsRoot()
  }
}

//cart cell
class CartCell:UITableViewCell{
  @IBOutlet var cartName:UILabel!
  override func awakeFromNib() {
  }
}

//
//  CreateCartController.swift
//  Q8Coders
//
//  Created by Afgan on 12/1/18.
//  Copyright © 2018 Afgan. All rights reserved.
//

import UIKit
import MBProgressHUD

class CreateCartController: UIViewController {
  
  @IBOutlet var cartTxtField: UITextField!
  var callback : ((NSDictionary)->Void)? // it sends data from add cart screen to cart list
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    cartTxtField.layer.cornerRadius = 12.0
    // Do any additional setup after loading the view.
  }
  @IBAction func doneButtonTapped(sender: UIBarButtonItem) {
    cartTxtField.resignFirstResponder()
    let cartName = cartTxtField.text ?? ""
    let ownerID = UserDefaults.standard.value(forKey: "owner_id") as? Int ?? 0
    if cartName == "" || ownerID == 0{ //validations for blank cart name
      self.view.makeToast("Please enter cart name")
      return
    }
    MBProgressHUD.showAdded(to: self.view, animated: true)
    AccountManager.inst.addNewCart(cartName: cartName, owner_id: "\(ownerID)", success: { (response) in
      Logger.log.debug("SUCCESS :: \(response)")
      
      MBProgressHUD.hide(for: self.view, animated: true)
      self.view.makeToast("cart added successfully")
      self.navigationController?.popViewController(animated: true)
      self.callback?(["name":cartName])
    }) { (error) in
      MBProgressHUD.hide(for: self.view, animated: true)
        self.view.makeToast(error == nil ? "Internet Connection Error" : "Invalid request")
      Logger.log.debug("ERROR :: \(String(describing: error))")
    }
  }
    //back button if user dnt want add cart
  @IBAction func backButtonTapped(sender: UIBarButtonItem) {
    navigationController?.popViewController(animated: true)
  }
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destination.
   // Pass the selected object to the new view controller.
   }
   */
  
}

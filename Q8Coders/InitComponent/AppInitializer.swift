//
//
//  Created by Gauhar Abbas on 01/12/18.
//  Copyright © 2018 Q8Coders. All rights reserved.

import Foundation
import UIKit

class AppInitializer {  
  let rootWireframe: RootWireframe = RootWireframe()
  func installRootViewControllerIntoWindow(window: UIWindow) {
        rootWireframe.window = window
  }
}


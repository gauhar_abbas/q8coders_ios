//
//
//  Created by Gauhar Abbas on 01/12/18.
//  Copyright © 2018 Q8Coders. All rights reserved.
//
import Foundation
import UIKit

let RegisterControllerIdentifier = "RegisterController"
let CartControllerIdentifier = "CartsController"

class RootWireframe {
    
    var window: UIWindow?
    var registerController: RegisterController?
    var cartController: CartsController?

    func showRootViewController(viewController: UIViewController) {
        if let window = window {
            if let navigationController = navigationControllerFromWindow(window: window) {
                navigationController.viewControllers = [viewController]
            } else {
                let navigationController = UINavigationController(rootViewController: viewController)
                window.rootViewController = navigationController
                navigationController.navigationBar.isHidden = true
                window.backgroundColor = UIColor.red
            }
        } else {
            Logger.log.error("ERROR: Window cannot be nil")
        }
    }
    
    private func navigationControllerFromWindow(window: UIWindow) -> UINavigationController? {
        if let navigationController = window.rootViewController as? UINavigationController {
            return navigationController
        } else {
            Logger.log.error("ERROR: No navigation controller found")
            return nil
        }
    }
    
    func showLoginModuleAsRoot() {
        registerController = UIStoryboard(name: "Main",
                                       bundle: Bundle(for: type(of: self))).instantiateViewController(withIdentifier: RegisterControllerIdentifier) as? RegisterController
        
        if let registerController = registerController {
            self.showRootViewController(viewController: registerController)
        } else {
            Logger.log.error("ERROR: View controller from storyboard not found")
        }
    }
  func showCartModuleAsRoot() {
    cartController = UIStoryboard(name: "Main",
                                      bundle: Bundle(for: type(of: self))).instantiateViewController(withIdentifier: CartControllerIdentifier) as? CartsController
    
    if let controller = cartController {
      self.showRootViewController(viewController: controller)
    } else {
      Logger.log.error("ERROR: View controller from storyboard not found")
    }
  }

    func showTabBarModuleAsRoot() {
        
    }
}



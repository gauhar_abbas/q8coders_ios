//
//  Created by Gauhar Abbas on 01/12/18.
//  Copyright © 2018 Q8Coders. All rights reserved.
//

import Foundation
import SwiftyJSON

public class AccountManager {
  
  let requestFactory = APIRequestFactory(NetworkManager.inst)
  
  public static var inst: AccountManager = {
    let instance = AccountManager()
    return instance
    
  }()
  
  private init() {
    
  }
  
  func registerPhone(country: String, phone : String,  success: @escaping(JSON) -> Void,
                     failure: @escaping(Error?) -> Void) {
    
    guard let request = requestFactory.register(country: country, phone: phone) else {
      failure(nil)
      return
    }
    NetworkManager.inst.request(request) { (json, error) in
      if let error = error {
        failure(error)
        return
      }
      if let json = json {
        success(json)
      } else {
        failure(nil)
      }
    }
    
  }
  func verifyOTP(phone:String, otp: String,  success: @escaping(JSON) -> Void,
                 failure: @escaping(Error?) -> Void) {
    
    guard let request = requestFactory.verify(phone:phone , code:otp )else {
      failure(nil)
      return
    }
    NetworkManager.inst.request(request) { (json, error) in
      if let error = error {
        failure(error)
        return
      }
      if let json = json {
        success(json)
      } else {
        failure(nil)
      }
    }
  }
  func addNewCart(cartName:String, owner_id: String,  success: @escaping(JSON) -> Void,
                  failure: @escaping(Error?) -> Void) {
    
    guard let request = requestFactory.addNewCarts(name:cartName,ownerId:owner_id) else {
      failure(nil)
      return
    }
    NetworkManager.inst.request(request) { (json, error) in
      if let error = error {
        failure(error)
        return
      }
      if let json = json {
        success(json)
      } else {
        failure(nil)
      }
    }
  }
  func getCartList(member_id:String, page_no: String,  success: @escaping(JSON) -> Void,
                   failure: @escaping(Error?) -> Void) {
    
    guard let url = requestFactory.getCartsList(member_id: member_id, pageNo: page_no) else {
      failure(nil)
      return
    }
    NetworkManager.inst.getRequest(url) { (json, error) in
      if let error = error {
        failure(error)
        return
      }
      if let json = json {
        success(json)
      } else {
        failure(nil)
      }
    }
  }
}

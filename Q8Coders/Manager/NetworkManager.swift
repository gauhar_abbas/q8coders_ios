//
//
//  Created by Gauhar Abbas on 01/12/18.
//  Copyright © 2018 Q8Coders. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

public class NetworkManager {
  
  var appEnvironment = AppEnvironment.prodEnvironment
  public static var inst: NetworkManager = {
    let instance = NetworkManager()
    return instance
    
  }()
  
  private init() {
    
  }
  
  /// Timeout interval to apply when making requests
  public static var timeoutInterval: TimeInterval = 60
  
  /// base URL for requests
  public var baseURL: URL? {
    
    get {
      if let urlString = self.appEnvironment.getServer(),
        let baseURL = URL(string: urlString) {
        return baseURL
      }
      return nil
    }
  }
  
  public func request(_ request: URLRequest,
                      completion: @escaping (JSON?, Error?) -> Void) {
    
    if !(NetworkReachabilityManager()?.isReachable)! {
      completion(nil, nil)
      return
    }
    Alamofire.request(request)
      .authenticate(user: "abstract", password: "anonymous")
      .validate(statusCode: 200..<300)
      .validate(contentType: ["application/json"])
      .responseJSON { response in
        
        debugPrint(response)
        
        switch response.result {
        case .success(let data):
          completion(JSON(data), nil)
          
        case .failure(let error):
          completion(nil, error)
          
        }
    }
  }
  public func getRequest(_ request: URL,
                         completion: @escaping (JSON?, Error?) -> Void) {
    
    if !(NetworkReachabilityManager()?.isReachable)! {
      completion(nil, nil)
      return
    }
    Alamofire.request(request)
      .authenticate(user: "abstract", password: "anonymous")
      .validate(statusCode: 200..<300)
      .validate(contentType: ["application/json"])
      .responseJSON { response in // method defaults to `.get`
        debugPrint(response)
        switch response.result {
        case .success(let data):
          completion(JSON(data), nil)
          
        case .failure(let error):
          completion(nil, error)
        }
    }
  }
}



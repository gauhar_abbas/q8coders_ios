//
//  ThemeManager.swift
//  Elixir
//
//  Created by Praveen Sharama on 15/10/18.
//  Copyright © 2018 Elixir. All rights reserved.
//

import Foundation
public class ThemeListener {
    
    public static var inst: ThemeManager = {
        let instance = ThemeManager()
        return instance
        
    }()
    
    private init() {
        
    }
}

//
//  DBManager.swift
//
//  Created by Gauhar Abbas on 01/12/18.
//  Copyright © 2018 Q8Coders. All rights reserved.
//

import Foundation

public class DBManager {
    
    public static var inst: DBManager = {
        let instance = DBManager()
        return instance
        
    }()
    
    private init() {
        
    }
}

//
//  NoteManager.swift
//  Elixir
//
//  Created by Praveen Sharama on 15/10/18.
//  Copyright © 2018 Elixir. All rights reserved.
//

import Foundation
public class NoteManager {
    
    public static var inst: NoteManager = {
        let instance = NoteManager()
        return instance
        
    }()
    
    private init() {
        
    }
}

//
//  SyncManager.swift
//  Elixir
//
//  Created by Praveen Sharama on 15/10/18.
//  Copyright © 2018 Elixir. All rights reserved.
//

import Foundation

public class SyncManager {
    
    public static var inst: SyncManager = {
        let instance = SyncManager()
        return instance
        
    }()
    
    private init() {
        
    }
}
